import org.junit.Test;
import resources.CalculatorResource;

import static org.junit.Assert.assertEquals;

public class CalculatorResourceTest{


    @Test
    public void testCalculate(){
        CalculatorResource calculatorResource = new CalculatorResource();

        String expression = "100+300";
        assertEquals("400", calculatorResource.calculate(expression));

        expression = " 300 - 99 ";
        assertEquals("201", calculatorResource.calculate(expression));

        expression = "2*3";
        assertEquals("6", calculatorResource.calculate(expression));

        expression = "2*3*56";
        assertEquals("336", calculatorResource.calculate(expression));

        expression = "6/2/1";
        assertEquals("3", calculatorResource.calculate(expression));
    }

    @Test
    public void testSum(){
        CalculatorResource calculatorResource = new CalculatorResource();

        String expression = "100+300+1";
        assertEquals("401", calculatorResource.sum(expression));

        expression = "300+99";
        assertEquals("399", calculatorResource.sum(expression));

        expression = "-300+99+0";
        assertEquals("-201", calculatorResource.sum(expression));

        expression = "-300+-99";
        assertEquals("-399", calculatorResource.sum(expression));
    }

    @Test
    public void testSubtraction(){
        CalculatorResource calculatorResource = new CalculatorResource();

        String expression = "999-100";
        assertEquals("899", calculatorResource.subtraction(expression));

        expression = "20-2";
        assertEquals("18", calculatorResource.subtraction(expression));

    }

    @Test
    public void testMultiplication(){
        CalculatorResource calculatorResource = new CalculatorResource();

        String expression = "7*-1";
        assertEquals("-7", calculatorResource.multiplication(expression));

        expression = "69*42";
        assertEquals("2898", calculatorResource.multiplication(expression));

        expression = "2*3*56";
        assertEquals("336", calculatorResource.calculate(expression));

        expression = "2*3*56*0";
        assertEquals("0", calculatorResource.calculate(expression));
    }

    @Test
    public void testDivision(){
        CalculatorResource calculatorResource = new CalculatorResource();

        String expression = "1050/50";
        assertEquals("21", calculatorResource.division(expression));

        expression = "30/-2/3";
        assertEquals("-5", calculatorResource.division(expression));

        expression = "0/35/90";
        assertEquals("0", calculatorResource.division(expression));

        expression = "50/0/70";
        assertEquals("Invalid input/divided by zero", calculatorResource.division(expression));
    }
}
